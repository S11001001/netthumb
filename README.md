# Thumbnail in .NET

Here is a function for producing thumbnails of a specific size, by
cropping from the center, as little as possible.

## License

netthumb is free, open source software, licensed under the MIT/X11
license.  A copy of the license is included in the file `LICENSE.md`.
