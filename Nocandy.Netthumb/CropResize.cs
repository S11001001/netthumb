﻿//
// CropResize.cs
//
// Author:
//       Stephen Compall <scompall@nocandysw.com>
//
// Copyright (c) 2015 Stephen Compall
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Drawing;
using System.IO;

namespace Nocandy.Netthumb
{

/// <summary>
/// A set of functions for cropping and downscaling; thumbnailing, in
/// other words.
/// </summary>
public sealed class CropResize
{
    /// <summary>
    /// Do not instantiate.
    /// </summary>
    private CropResize () {}

    /// <summary>
    /// Crops a file.  A simple convenience wrapper for <see cref="CropImage"/>.
    /// <para>Many other such wrappers are possible, but not defined
    ///     here.  It is relatively straightforward to derive an
    ///     argument to <see cref="CropImage"/> and handle its result
    ///     in a way that fits with your image storage model.</para>
    /// </summary>
    /// <param name="infile">Path to the input image file.  Must be in
    ///     an input format that <see cref="Image.FromFile"/>
    ///     understands.</param>
    /// <param name="outfile">Path to where the output image will be 
    ///     written.  Must be an output format that
    ///     <see cref="Image.Save"/> understands.</param>
    /// <param name="conf">General appearance of thumbnails for this 
    ///     thumb set.</param>
    /// <seealso cref="CropImage"/>
    /// <seealso cref="Image.FromFile"/> 
    /// <seealso cref="Image.Save"/> 
    public static void CropFile(
        string infile, string outfile,
        ThumbStyle conf)
    {
        CropImage (Image.FromFile (infile), conf).Save(outfile);
    }

    public static Image CropImage(
        Image inimg, ThumbStyle conf)
    {
        var calculated = ChooseCropRegions(inimg.Width, inimg.Height, conf);
        Image outimg = new Bitmap (calculated.Item1, calculated.Item2);
        using (Graphics g = Graphics.FromImage (outimg)) {
            g.DrawImage (inimg,
                destRect: new Rectangle (x: 0, y: 0, width: calculated.Item1, 
                                         height: calculated.Item2),
                srcRect: calculated.Item3,
                srcUnit: GraphicsUnit.Pixel);
        }
        return outimg;
    }

    /// <summary>
    /// Find final dimensions and crop region for conf of an image of
    /// inWidth x inHeight.
    /// </summary>
    /// <returns>The crop regions.</returns>
    /// <param name="inWidth">Original image width.</param>
    /// <param name="inHeight">Original image height.</param>
    /// <param name="conf">General appearance of thumbnails for this
    ///     thumb set.</param>
    private static Tuple<int, int, Rectangle> ChooseCropRegions(
        int inWidth, int inHeight, ThumbStyle conf)
    {
        var mw = Min (inWidth, conf.MaxWidth);
        var mh = Min (inHeight, conf.MaxHeight);
        var outRatio = Convert.ToDecimal (mw) / mh;
        int iw, ih;
        if (outRatio >= Convert.ToDecimal (inWidth) / inHeight) {
            // landscape or same-ratio, crop top/bottom
            iw = inWidth;
            ih = (int) (inWidth / outRatio);
        } else {
            // portrait, crop left/right
            ih = inHeight;
            iw = (int) (inHeight * outRatio);
        }
        return Tuple.Create (
            mw, mh,
            new Rectangle (x: (inWidth - iw) / 2, y: (inHeight - ih) / 2, 
                           width: iw, height: ih));
    }

    private static int Min (int l, int r)
    {
        return l < r ? l : r;
    }
}
}

